'use strict';
var randomize = require('randomatic');


module.exports = function (Licencerequest) {
  Licencerequest.observe('before save', function (ctx, next) {
    if (ctx.isNewInstance) {
      getUniqueNumber(function (randomNumber) {

        ctx.instance.licenceId = randomNumber;
        ctx.instance.expireDate=new Date('07/31/2018');
        next(null, ctx);

      })

      function getUniqueNumber(cb) {
        var randomNumber = randomize('0', 8);
        //var randomNumber = 51223053;
        console.log('randomNumber ' + randomNumber);
        Licencerequest.find({
          where: {
            licenceId:
            randomNumber
          }
        }, function (err, data) {
          if (data.length == 0) {
            cb(randomNumber);
          }
        })
      }

    } else {
      next(null, ctx);
    }

  })
};
